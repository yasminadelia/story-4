from django import forms
from django.forms import Form
from django.utils import timezone


CATEGORY_CHOICES = [
    ('Academic','Academic'),
    ('Entertainment','Entertainment'),
    ('Family','Family'),
    ('Friends','Friends'),
    ('Organization','Organization'),
    ('Other','Other')
    ]

class Event_Form(forms.Form):

    pos_attrs = {
        'class': 'form-control',
        
    }
    category = forms.CharField(label='Category', required=True, widget=forms.Select(choices=CATEGORY_CHOICES))
    event_name = forms.CharField(label='Name', required=True, max_length=40, widget=forms.TextInput(attrs=pos_attrs))
    date_time = forms.DateTimeField(
                input_formats=['%d/%m/%Y %H:%M'],
                widget=forms.DateTimeInput(attrs={
                'class': 'form-control datetimepicker-input',
                    'data-target': '#datetimepicker1'
                })
                )
    # start_time = forms.TimeField(label="Time",required=False)
    location = forms.CharField(label="Location", required=False)
    notes = forms.CharField(label="Notes", widget=forms.Textarea(attrs=pos_attrs), required=False, max_length=150)

