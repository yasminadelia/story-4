from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.utils import timezone
# Create your models here.

class Event(models.Model):
	category = models.CharField(blank=False, max_length=20)
	event_name = models.CharField(blank=False, max_length=30)
	date_time = models.DateTimeField(blank=False, default=timezone.now)
	# start_time = models.TimeField(blank=True, null=True)
	location = models.TextField(blank=True, null=True)
	notes = models.TextField(blank=True, null=True, max_length=150)

	def __str__(self):
		return self.event_name

    # class Meta:
    #     verbose_name = 'Scheduling'
    #     verbose_name_plural = 'Scheduling'

# class Person(models.Model):
# 	email =  models.CharField(max_length=30)

# class Buku(models.Model):
# 	judul =  models.CharField(max_length=30)
# 	author =  models.CharField(max_length=30)




