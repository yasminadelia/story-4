from django.urls import include, path
from . import views
from .views import all_event, add_event, delete_all, delete_event
app_name = 'my_profile'

urlpatterns = [
	path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('resume/', views.resume, name='resume'),
    path('connect/', views.connect, name='connect'),
    path('gallery/', views.gallery, name='gallery'),
   

    path('all_event/', views.all_event, name='all_event'),
    path('add_event/', views.add_event, name='add_event'),
    path('delete_all/', views.delete_all, name='delete_all'),
    path('delete_event/<int:pk>', views.delete_event, name='delete_event'),

    
]